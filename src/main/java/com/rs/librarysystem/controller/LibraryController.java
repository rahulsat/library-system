package com.rs.librarysystem.controller;

import com.rs.librarysystem.domain.Response;
import com.rs.librarysystem.dto.BookDTO;
import com.rs.librarysystem.dto.BorrowerDTO;
import com.rs.librarysystem.entity.Book;
import com.rs.librarysystem.service.LibraryService;
import com.rs.librarysystem.util.EndPoint;
import com.rs.librarysystem.util.Utility;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@RestController
@RequestMapping("/api/v1/library")
public class LibraryController {

    private static final Logger logger = LoggerFactory.getLogger(LibraryController.class);
    private final LibraryService libraryService;

    @Autowired
    public LibraryController(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

    @PostMapping(value = EndPoint.REGISTER_BORROWER, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "New borrower registration", description = "Register a new borrower to the library")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successfully registered borrower", content = @Content(schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "400", description = "Bad request"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    public ResponseEntity<Object> borrowerRegistration(@Valid @RequestBody BorrowerDTO borrowerDTO, Locale locale) {
        logger.debug("Received borrower registration request - {} ", Utility.objectToJson(borrowerDTO));
        return libraryService.borrowerRegistration(borrowerDTO, locale);
    }

    @PostMapping(value = EndPoint.REGISTER_BOOK, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Register a new book", description = "Register a new book in the library")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successfully registered book", content = @Content(schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "400", description = "Bad request"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    public ResponseEntity<Object> registerBook(@Valid @RequestBody BookDTO bookDTO, Locale locale) {
        logger.debug("Received book registration request - {} ", Utility.objectToJson(bookDTO));
        return libraryService.registerBook(bookDTO, locale);
    }

    @GetMapping(value = EndPoint.GET_ALL_BOOKS, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get all books", description = "Retrieve a list of all books in the library")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successfully retrieved list of books", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Book.class)))),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    public ResponseEntity<Object> getAllBooks(Locale locale) {
        logger.debug("Received request to get all books");
        return libraryService.getAllBooks(locale);
    }

    @PostMapping(value = EndPoint.BORROW_BOOK, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Borrow a book", description = "Borrow a book from the library")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successfully borrowed book", content = @Content(schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "400", description = "Bad request"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    public ResponseEntity<Object> borrowBook(@RequestParam int bookId, @RequestParam int borrowerId, Locale locale) {
        logger.debug("Received borrow book request - bookId: {}, borrowerId: {}", bookId, borrowerId);
        return libraryService.borrowBook(bookId, borrowerId, locale);
    }

    @PostMapping(value = EndPoint.RETURN_BOOK, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Return a borrowed book", description = "Return a borrowed book to the library")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successfully returned book", content = @Content(schema = @Schema(implementation = Response.class))),
            @ApiResponse(responseCode = "400", description = "Bad request"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    public ResponseEntity<Object> returnBook(@RequestParam int recordId, Locale locale) {
        logger.debug("Received return book request - recordId: {}", recordId);
        return libraryService.returnBook(recordId, locale);
    }
}
