package com.rs.librarysystem.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Response {
    private String status;
    private String message;
    private Object data;

    public Response(String s, String success) {
        this.status=s;
        this.message=success;
    }

}
