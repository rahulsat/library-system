package com.rs.librarysystem.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookDTO {
    @NotBlank(message = "isbn.mandatory")
    @Size(min = 10, max = 14, message = "isbn.size")
    private String isbn;

    @NotBlank(message = "title.mandatory")
    @Size(min = 1, max = 100, message = "title.size")
    private String title;

    private int author;
    private int available;
}
