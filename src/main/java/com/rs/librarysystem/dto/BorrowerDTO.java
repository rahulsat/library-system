package com.rs.librarysystem.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BorrowerDTO {
    @NotBlank(message = "name.notBlank")
    @Size(min = 2, max = 50, message = "name.size")
    private String name;

    @Email(message = "email.valid")
    @NotBlank(message = "email.notBlank")
    private String email;
}
