package com.rs.librarysystem.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "book")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BookID")
    private int bookID;
    @Column(name = "ISBN", unique = true)
    private String isbn;
    @Column(name = "Title")
    private String title;
    @ManyToOne
    @JoinColumn(name = "AuthorID", nullable = false)
    private Author author;
    @Column(name = "Available", nullable = false)
    private int available;
}
