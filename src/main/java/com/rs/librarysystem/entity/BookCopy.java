package com.rs.librarysystem.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "book_copy")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookCopy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CopyID")
    private int copyID;
    @ManyToOne
    @JoinColumn(name = "BookID", nullable = false)
    private Book book;
    @Enumerated(EnumType.STRING)
    @Column(name = "Status", nullable = false)
    private BookCopyStatus status;
}
