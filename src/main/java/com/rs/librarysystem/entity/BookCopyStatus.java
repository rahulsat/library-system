package com.rs.librarysystem.entity;

public enum BookCopyStatus {
    AVAILABLE,
    BORROWED,
    RESERVED,
    LOST
}
