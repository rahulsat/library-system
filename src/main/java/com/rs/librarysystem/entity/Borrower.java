package com.rs.librarysystem.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "borrower")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Borrower {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BorrowerID")
    private int borrowerID;
    @Column(name = "Name")
    private String name;
    @Column(unique = true, name = "Email")
    private String email;
}