package com.rs.librarysystem.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Table(name = "borrowing_record")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BorrowingRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RecordID")
    private int recordID;
    @ManyToOne
    @JoinColumn(name = "BorrowerID", nullable = false)
    private Borrower borrower;
    @ManyToOne
    @JoinColumn(name = "CopyID", nullable = false)
    private BookCopy bookCopy;
    @Temporal(TemporalType.DATE)
    @Column(name = "Borrowed_Date")
    private Date borrowedDate;
    @Temporal(TemporalType.DATE)
    @Column(name = "Due_Date")
    private Date dueDate;
    @Temporal(TemporalType.DATE)
    @Column(name = "Returned_Date")
    private Date returnedDate;
}
