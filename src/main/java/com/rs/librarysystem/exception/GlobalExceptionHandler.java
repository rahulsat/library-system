package com.rs.librarysystem.exception;

import com.rs.librarysystem.domain.FieldValidationError;
import com.rs.librarysystem.util.ResponseCode;
import com.rs.librarysystem.util.ResponseGenerator;
import com.rs.librarysystem.util.ResponseMessage;
import com.rs.librarysystem.util.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@ControllerAdvice
public class GlobalExceptionHandler {

    private final ResponseGenerator responseGenerator;
    private final MessageSource messageSource;
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @Autowired
    public GlobalExceptionHandler(ResponseGenerator responseGenerator, MessageSource messageSource) {
        this.responseGenerator = responseGenerator;
        this.messageSource = messageSource;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex, Locale locale) {
        List<FieldValidationError> fieldValidationErrorList = new ArrayList<>();

        ex.getBindingResult().getFieldErrors().forEach(fieldError -> {
            String fieldName = fieldError.getField();
            String errorMessage = messageSource.getMessage(fieldError.getDefaultMessage(), new Object[]{}, locale);
            fieldValidationErrorList.add(new FieldValidationError(fieldName, errorMessage));
        });

        logger.error("Field error - {} ", Utility.objectToJson(fieldValidationErrorList));
        return responseGenerator.generateResponse(fieldValidationErrorList, ResponseCode.METHOD_ARGUMENT_NOTVALID, ResponseMessage.REQUIRED_DATA_ELEMENT_MISSING_MSG, new Object[]{}, locale);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<?> handleNoHandlerFoundException(NoHandlerFoundException e, Locale locale) {
        logger.error("No handler found", e);
        return responseGenerator.generateResponse(ResponseCode.RESOURCE_NOT_FOUND, ResponseMessage.RESOURCE_NOT_FOUND_MSG, new Object[]{}, locale);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<?> handleHttpMessageNotReadableException(HttpMessageNotReadableException e, Locale locale) {
        logger.error("Message not readable", e);
        return responseGenerator.generateResponse(ResponseCode.MESSAGE_NOTREADABLE, ResponseMessage.MESSAGE_NOTREADABLE_MSG, new Object[]{}, locale);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> handleIllegalArgumentException(IllegalArgumentException e, Locale locale) {
        logger.error("Illegal argument", e);
        return responseGenerator.generateResponse(ResponseCode.ILLEGAL_ARGUMENT, ResponseMessage.ILLEGAL_ARGUMENT_MSG, new Object[]{}, locale);
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<?> handleNullPointerException(NullPointerException e, Locale locale) {
        logger.error("Null pointer exception", e);
        return responseGenerator.generateResponse(ResponseCode.NULL_POINTER, ResponseMessage.NULL_POINTER_MSG, new Object[]{}, locale);
    }

    @ExceptionHandler(ConnectException.class)
    public ResponseEntity<?> handleConnectException(ConnectException e, Locale locale) {
        logger.error("Connect exception", e);
        return responseGenerator.generateResponse(ResponseCode.CONNECT_EXCEPTION, ResponseMessage.CONNECT_EXCEPTION_MSG, new Object[]{}, locale);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception e, Locale locale) {
        logger.error("System error", e);
        return responseGenerator.generateResponse(ResponseCode.FAILED, ResponseMessage.SYSTEM_ERROR_MSG, new Object[]{}, locale);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException e, Locale locale) {
        logger.error("Resource not found", e);
        return responseGenerator.generateResponse(ResponseCode.ENTITY_NOT_FOUND, e.getMessage(), new Object[]{}, locale);
    }
}
