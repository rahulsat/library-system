package com.rs.librarysystem.repository;

import com.rs.librarysystem.entity.Book;
import com.rs.librarysystem.entity.BookCopy;
import com.rs.librarysystem.entity.BookCopyStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BookCopyRepository extends JpaRepository<BookCopy, Integer> {

    Optional<BookCopy> findFirstByBookAndStatus(Book book, BookCopyStatus status);
}