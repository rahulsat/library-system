package com.rs.librarysystem.repository;

import com.rs.librarysystem.entity.Borrower;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BorrowerRepository extends JpaRepository<Borrower, Integer> {

    Optional<Borrower> findByEmail(String email);
}
