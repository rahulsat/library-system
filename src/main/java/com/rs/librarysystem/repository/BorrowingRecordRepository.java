package com.rs.librarysystem.repository;

import com.rs.librarysystem.entity.BorrowingRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BorrowingRecordRepository extends JpaRepository<BorrowingRecord, Integer> {

    @Query("SELECT COUNT(br) FROM BorrowingRecord br WHERE br.borrower.borrowerID = :borrowerId AND br.returnedDate IS NULL")
    int countBorrowedBooksByBorrower(int borrowerId);
}
