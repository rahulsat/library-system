package com.rs.librarysystem.service;


import com.rs.librarysystem.dto.BookDTO;
import com.rs.librarysystem.dto.BorrowerDTO;
import com.rs.librarysystem.entity.Book;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Locale;

public interface LibraryService {
    ResponseEntity<Object> borrowerRegistration(BorrowerDTO borrowerDTO, Locale locale);
    ResponseEntity<Object> registerBook(BookDTO bookDTO, Locale locale);
    ResponseEntity<Object> getAllBooks(Locale locale);
    ResponseEntity<Object> borrowBook(int bookId, int borrowerId, Locale locale);
    ResponseEntity<Object> returnBook(int recordId, Locale locale);
}
