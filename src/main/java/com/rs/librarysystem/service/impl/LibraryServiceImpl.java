package com.rs.librarysystem.service.impl;

import com.rs.librarysystem.dto.BookDTO;
import com.rs.librarysystem.dto.BorrowerDTO;
import com.rs.librarysystem.entity.*;
import com.rs.librarysystem.repository.*;
import com.rs.librarysystem.service.LibraryService;
import com.rs.librarysystem.util.ResponseCode;
import com.rs.librarysystem.util.ResponseGenerator;
import com.rs.librarysystem.util.ResponseMessage;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class LibraryServiceImpl implements LibraryService {

    private final BorrowerRepository borrowerRepository;
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final BorrowingRecordRepository borrowingRecordRepository;
    private final BookCopyRepository bookCopyRepository;
    private final ResponseGenerator responseGenerator;
    private final ModelMapper modelMapper;

    @Value("${book.borrow.due.days}")
    private int dueDays;

    @Value("${book.borrow.limit}")
    private int borrowLimit;

    @Autowired
    public LibraryServiceImpl(BorrowerRepository borrowerRepository, BookRepository bookRepository, AuthorRepository authorRepository, BorrowingRecordRepository borrowingRecordRepository, BookCopyRepository bookCopyRepository, ResponseGenerator responseGenerator, ModelMapper modelMapper) {
        this.borrowerRepository = borrowerRepository;
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.borrowingRecordRepository = borrowingRecordRepository;
        this.bookCopyRepository = bookCopyRepository;
        this.responseGenerator = responseGenerator;
        this.modelMapper = modelMapper;
    }

    @Override
    public ResponseEntity<Object> borrowerRegistration(BorrowerDTO borrowerDTO, Locale locale) {
        Optional<Borrower> existingBorrower = borrowerRepository.findByEmail(borrowerDTO.getEmail());

        if (existingBorrower.isEmpty()) {
            Borrower newBorrower = modelMapper.map(borrowerDTO, Borrower.class);
            borrowerRepository.save(newBorrower);
            return responseGenerator.generateResponse(ResponseCode.SUCCESS, ResponseMessage.BORROWER_REGISTRATION_SUCCESS_MSG, new Object[]{}, locale);
        }

        return responseGenerator.generateResponse(ResponseCode.EMAIL_ALREADY_REGISTERED, ResponseMessage.EMAIL_ALREADY_REGISTERED_MSG, new Object[]{}, locale);
    }

    @Override
    @Transactional
    public ResponseEntity<Object> registerBook(BookDTO bookDTO, Locale locale) {
        Optional<Book> existingBook = bookRepository.findByIsbn(bookDTO.getIsbn());

        if (existingBook.isEmpty()) {
            Optional<Author> author = authorRepository.findById(bookDTO.getAuthor());

            if (!author.isEmpty()) {
                Book newBook = modelMapper.map(bookDTO, Book.class);
                newBook.setAuthor(author.get());
                bookRepository.save(newBook);

                // Add copies to bookCopy table
                if (bookDTO.getAvailable() > 0) {
                    for (int i = 0; i < bookDTO.getAvailable(); i++) {
                        BookCopy bookCopy = new BookCopy();
                        bookCopy.setBook(newBook);
                        bookCopy.setStatus(BookCopyStatus.AVAILABLE);
                        bookCopyRepository.save(bookCopy);
                    }
                }

                return responseGenerator.generateResponse(ResponseCode.SUCCESS, ResponseMessage.BOOK_REGISTRATION_SUCCESS_MSG, new Object[]{}, locale);
            }

            return responseGenerator.generateResponse(ResponseCode.INVALID_AUTHOR, ResponseMessage.ERROR_INVALID_AUTHOR_DETAILS, new Object[]{}, locale);
        }

        return responseGenerator.generateResponse(ResponseCode.BOOK_ALREADY_REGISTERED, ResponseMessage.BOOK_ALREADY_REGISTERED_MSG, new Object[]{}, locale);
    }

    @Override
    public ResponseEntity<Object> getAllBooks(Locale locale) {
        List<Book> books = bookRepository.findAll();
        return responseGenerator.generateResponse(books, ResponseCode.SUCCESS, ResponseMessage.BOOK_REGISTRATION_SUCCESS_MSG, new Object[]{}, locale);
    }

    @Override
    @Transactional
    public ResponseEntity<Object> borrowBook(int bookId, int borrowerId, Locale locale) {
        Optional<Book> bookOptional = bookRepository.findById(bookId);
        Optional<Borrower> borrowerOptional = borrowerRepository.findById(borrowerId);

        if (bookOptional.isPresent() && borrowerOptional.isPresent()) {
            Book book = bookOptional.get();
            Borrower borrower = borrowerOptional.get();

            int borrowedBooksCount = borrowingRecordRepository.countBorrowedBooksByBorrower(borrowerId);
            if (borrowedBooksCount >= borrowLimit)
                return responseGenerator.generateResponse(ResponseCode.LIMIT_EXCEEDED, ResponseMessage.BORROW_LIMIT_EXCEEDED, new Object[]{}, locale);

            if (book.getAvailable() > 0) {
                // Find an available copy of the book
                Optional<BookCopy> bookCopyOptional = bookCopyRepository.findFirstByBookAndStatus(book, BookCopyStatus.AVAILABLE);

                if (bookCopyOptional.isPresent()) {
                    BookCopy bookCopy = bookCopyOptional.get();

                    BorrowingRecord borrowingRecord = new BorrowingRecord();
                    borrowingRecord.setBorrower(borrower);
                    borrowingRecord.setBookCopy(bookCopy);
                    borrowingRecord.setBorrowedDate(new Date());
                    borrowingRecord.setDueDate(new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(dueDays))); // Use configured due date
                    borrowingRecordRepository.save(borrowingRecord);

                    bookCopy.setStatus(BookCopyStatus.BORROWED);
                    bookCopyRepository.save(bookCopy);

                    book.setAvailable(book.getAvailable() - 1);
                    bookRepository.save(book);

                    return responseGenerator.generateResponse(ResponseCode.SUCCESS, ResponseMessage.BOOK_BORROWED_SUCCESS_MSG, new Object[]{}, locale);
                } else {
                    return responseGenerator.generateResponse(ResponseCode.NO_COPIES_AVAILABLE, ResponseMessage.NO_COPIES_AVAILABLE_MSG, new Object[]{}, locale);
                }
            } else {
                return responseGenerator.generateResponse(ResponseCode.NO_COPIES_AVAILABLE, ResponseMessage.NO_COPIES_AVAILABLE_MSG, new Object[]{}, locale);
            }
        }

        return responseGenerator.generateResponse(ResponseCode.INVALID_BOOK_OR_BORROWER, ResponseMessage.INVALID_BOOK_OR_BORROWER_MSG, new Object[]{}, locale);
    }

    @Override
    @Transactional
    public ResponseEntity<Object> returnBook(int recordId, Locale locale) {
        Optional<BorrowingRecord> recordOptional = borrowingRecordRepository.findById(recordId);

        if (recordOptional.isPresent()) {
            BorrowingRecord borrowingRecord = recordOptional.get();
            borrowingRecord.setReturnedDate(new Date());
            borrowingRecordRepository.save(borrowingRecord);

            BookCopy bookCopy = borrowingRecord.getBookCopy();
            bookCopy.setStatus(BookCopyStatus.AVAILABLE);
            bookCopyRepository.save(bookCopy);

            Book book = bookCopy.getBook();
            book.setAvailable(book.getAvailable() + 1);
            bookRepository.save(book);

            return responseGenerator.generateResponse(ResponseCode.SUCCESS, ResponseMessage.BOOK_RETURNED_SUCCESS_MSG, new Object[]{}, locale);
        }

        return responseGenerator.generateResponse(ResponseCode.INVALID_RECORD, ResponseMessage.INVALID_RECORD_MSG, new Object[]{}, locale);
    }
}
