package com.rs.librarysystem.util;

public class EndPoint {

    public static final String REGISTER_BORROWER = "/registerBorrower";

    public static final String REGISTER_BOOK = "/registerBook";

    public static final String GET_ALL_BOOKS = "/getAllBooks";

    public static final String BORROW_BOOK = "/borrowBook";

    public static final String RETURN_BOOK = "/returnBook";

}
