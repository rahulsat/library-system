package com.rs.librarysystem.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ResponseCode {
    public static final String SUCCESS = "00";
    public static final String FAILED = "01";
    public static final String METHOD_ARGUMENT_NOTVALID = "02";
    public static final String RESOURCE_NOT_FOUND = "03";
    public static final String ENTITY_NOT_FOUND = "04";
    public static final String MESSAGE_NOTREADABLE = "05";
    public static final String ILLEGAL_ARGUMENT = "06";
    public static final String NULL_POINTER = "07";
    public static final String CONNECT_EXCEPTION = "08";

    //System Error Codes
    public static final String EMAIL_ALREADY_REGISTERED = "09";
    public static final String BOOK_ALREADY_REGISTERED = "10";
    public static final String INVALID_AUTHOR= "11";
    public static final String NO_COPIES_AVAILABLE= "12";
    public static final String INVALID_BOOK_OR_BORROWER= "13";
    public static final String INVALID_RECORD= "14";
    public static final String LIMIT_EXCEEDED= "15";



}
