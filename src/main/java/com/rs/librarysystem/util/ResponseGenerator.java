package com.rs.librarysystem.util;

import com.rs.librarysystem.domain.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class ResponseGenerator {

    private static final Logger logger = LoggerFactory.getLogger(ResponseGenerator.class);
    private final MessageSource messageSource;

    @Autowired
    public ResponseGenerator(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public ResponseEntity<Object> generateResponse(String responseCode, String messagePropertyName, Object[] params, Locale locale) {
        Response response = new Response();
        response.setStatus(responseCode);
        response.setMessage(messageSource.getMessage(messagePropertyName, params, locale));

        logger.debug("{} - {}", responseCode, response.getMessage());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    public ResponseEntity<Object> generateResponse(Object data, String responseCode, String messagePropertyName, Object[] params, Locale locale) {
        Response response = new Response();
        response.setStatus(responseCode);
        response.setMessage(messageSource.getMessage(messagePropertyName, params, locale));
        response.setData(data);

        logger.debug("{} - {}", responseCode, response.getMessage());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}