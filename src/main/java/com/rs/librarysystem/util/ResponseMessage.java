package com.rs.librarysystem.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ResponseMessage {

    //Generic Error Messages
    public static final String RESOURCE_NOT_FOUND_MSG = "resource.not.found";
    public static final String SYSTEM_ERROR_MSG = "system.error";
    public static final String REQUIRED_DATA_ELEMENT_MISSING_MSG = "mandatory.fields.missing";
    public static final String MESSAGE_NOTREADABLE_MSG = "message.notreadable";
    public static final String ILLEGAL_ARGUMENT_MSG = "illegal.argument";
    public static final String NULL_POINTER_MSG = "null.pointer";
    public static final String CONNECT_EXCEPTION_MSG = "connect.exception";


    //System Error Messages
    public static final String BORROWER_REGISTRATION_SUCCESS_MSG = "borrower.registration.success";
    public static final String EMAIL_ALREADY_REGISTERED_MSG = "email.already.registered";
    public static final String BOOK_REGISTRATION_SUCCESS_MSG = "book.registration.success";
    public static final String BOOK_ALREADY_REGISTERED_MSG = "book.already.registered";
    public static final String ERROR_INVALID_AUTHOR_DETAILS  = "error.invalid.author.details";

    // New Messages for Borrow and Return Operations
    public static final String BOOK_BORROWED_SUCCESS_MSG = "book.borrowed.success";
    public static final String NO_COPIES_AVAILABLE_MSG = "no.copies.available";
    public static final String INVALID_BOOK_OR_BORROWER_MSG = "invalid.book.or.borrower";
    public static final String BOOK_RETURNED_SUCCESS_MSG = "book.returned.success";
    public static final String INVALID_RECORD_MSG = "invalid.record";
    public static final String BORROW_LIMIT_EXCEEDED = "borrow.limit.exceeded";


}
