package com.rs.librarysystem;

import com.rs.librarysystem.entity.Author;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AuthorTest {

    @Test
    void testAuthorEntity() {
        Author author = new Author();
        author.setAuthorID(1);
        author.setName("Author Name");

        assertEquals(1, author.getAuthorID());
        assertEquals("Author Name", author.getName());
    }

    @Test
    void testAuthorEqualsHashCode() {
        Author author1 = new Author(1, "Author Name");
        Author author2 = new Author(1, "Author Name");

        assertEquals(author1, author2);
        assertEquals(author1.hashCode(), author2.hashCode());
    }
}
