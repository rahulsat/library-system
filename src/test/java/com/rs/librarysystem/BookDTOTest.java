package com.rs.librarysystem;

import com.rs.librarysystem.dto.BookDTO;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BookDTOTest {

    @Test
    void testBookDTO() {
        BookDTO bookDTO = new BookDTO();
        bookDTO.setIsbn("123456789");
        bookDTO.setTitle("Spring Boot");
        bookDTO.setAuthor(1);
        bookDTO.setAvailable(5);

        assertEquals("123456789", bookDTO.getIsbn());
        assertEquals("Spring Boot", bookDTO.getTitle());
        assertEquals(1, bookDTO.getAuthor());
        assertEquals(5, bookDTO.getAvailable());
    }

    @Test
    void testBookDTOEqualsHashCode() {
        BookDTO bookDTO1 = new BookDTO("123456789", "Spring Boot", 1, 5);
        BookDTO bookDTO2 = new BookDTO("123456789", "Spring Boot", 1, 5);

        assertEquals(bookDTO1, bookDTO2);
        assertEquals(bookDTO1.hashCode(), bookDTO2.hashCode());
    }
}