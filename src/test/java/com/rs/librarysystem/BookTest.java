package com.rs.librarysystem;

import com.rs.librarysystem.entity.Author;
import com.rs.librarysystem.entity.Book;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BookTest {

    @Test
    void testBookEntity() {
        Author author = new Author(1, "Author Name");

        Book book = new Book();
        book.setBookID(1);
        book.setIsbn("123456789");
        book.setTitle("Spring Boot");
        book.setAuthor(author);
        book.setAvailable(5);

        assertEquals(1, book.getBookID());
        assertEquals("123456789", book.getIsbn());
        assertEquals("Spring Boot", book.getTitle());
        assertEquals(author, book.getAuthor());
        assertEquals(5, book.getAvailable());
    }

    @Test
    void testBookEqualsHashCode() {
        Author author = new Author(1, "Author Name");
        Book book1 = new Book(1, "123456789", "Spring Boot", author, 5);
        Book book2 = new Book(1, "123456789", "Spring Boot", author, 5);

        assertEquals(book1, book2);
        assertEquals(book1.hashCode(), book2.hashCode());
    }
}