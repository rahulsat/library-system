package com.rs.librarysystem;

import com.rs.librarysystem.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CustomExceptionTest {

    @Test
    void testCustomException() {
        ResourceNotFoundException exception = new ResourceNotFoundException("Custom error message");

        assertEquals("Custom error message", exception.getMessage());
    }
}
