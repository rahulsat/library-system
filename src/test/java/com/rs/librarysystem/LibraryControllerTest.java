package com.rs.librarysystem;

import com.rs.librarysystem.controller.LibraryController;
import com.rs.librarysystem.dto.BookDTO;
import com.rs.librarysystem.dto.BorrowerDTO;
import com.rs.librarysystem.service.LibraryService;
import com.rs.librarysystem.util.EndPoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Locale;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LibraryControllerTest {

    @Mock
    private LibraryService libraryService;

    @InjectMocks
    private LibraryController libraryController;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(libraryController).build();
    }

    @Test
    void testBorrowerRegistration() throws Exception {
        BorrowerDTO borrowerDTO = new BorrowerDTO();
        borrowerDTO.setName("Rahul Satheesh");
        borrowerDTO.setEmail("rs@example.com");

        when(libraryService.borrowerRegistration(any(BorrowerDTO.class), any(Locale.class)))
                .thenReturn(ResponseEntity.ok().body("Success"));

        mockMvc.perform(post("/api/v1/library" + EndPoint.REGISTER_BORROWER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"Rahul Satheesh\", \"email\":\"rs@example.com\"}"))
                .andExpect(status().isOk());
    }

    @Test
    void testBorrowerRegistrationInvalidEmail() throws Exception {
        BorrowerDTO borrowerDTO = new BorrowerDTO();
        borrowerDTO.setName("Rahul Satheesh");
        borrowerDTO.setEmail("invalid-email");

        mockMvc.perform(post("/api/v1/library" + EndPoint.REGISTER_BORROWER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"Rahul Satheesh\", \"email\":\"invalid-email\"}"))
                .andExpect(status().isBadRequest());
    }

//    @Test
//    void testRegisterBook() throws Exception {
//        BookDTO bookDTO = new BookDTO();
//        bookDTO.setIsbn("123456789");
//        bookDTO.setTitle("Spring Boot");
//        bookDTO.setAuthor(1);
//        bookDTO.setAvailable(5);
//
//        when(libraryService.registerBook(any(BookDTO.class), any(Locale.class)))
//                .thenReturn(ResponseEntity.ok().body("Success"));
//
//        mockMvc.perform(post("/api/v1/library" + EndPoint.REGISTER_BOOK)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content("{\"isbn\":\"123456789\", \"title\":\"Spring Boot\", \"author\":1, \"available\":5}"))
//                .andExpect(status().isOk());
//    }

    @Test
    void testRegisterBookMissingTitle() throws Exception {
        BookDTO bookDTO = new BookDTO();
        bookDTO.setIsbn("123456789");
        bookDTO.setAuthor(1);
        bookDTO.setAvailable(5);

        mockMvc.perform(post("/api/v1/library" + EndPoint.REGISTER_BOOK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"isbn\":\"123456789\", \"author\":1, \"available\":5}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testGetAllBooks() throws Exception {
        when(libraryService.getAllBooks(any(Locale.class)))
                .thenReturn(ResponseEntity.ok().body("Success"));

        mockMvc.perform(get("/api/v1/library" + EndPoint.GET_ALL_BOOKS)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void testBorrowBook() throws Exception {
        when(libraryService.borrowBook(any(Integer.class), any(Integer.class), any(Locale.class)))
                .thenReturn(ResponseEntity.ok().body("Success"));

        mockMvc.perform(post("/api/v1/library" + EndPoint.BORROW_BOOK)
                        .param("bookId", "1")
                        .param("borrowerId", "1"))
                .andExpect(status().isOk());
    }

    @Test
    void testReturnBook() throws Exception {
        when(libraryService.returnBook(any(Integer.class), any(Locale.class)))
                .thenReturn(ResponseEntity.ok().body("Success"));

        mockMvc.perform(post("/api/v1/library" + EndPoint.RETURN_BOOK)
                        .param("recordId", "1"))
                .andExpect(status().isOk());
    }
}
