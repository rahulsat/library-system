package com.rs.librarysystem;

import com.rs.librarysystem.dto.BookDTO;
import com.rs.librarysystem.dto.BorrowerDTO;
import com.rs.librarysystem.entity.*;
import com.rs.librarysystem.repository.*;
import com.rs.librarysystem.service.impl.LibraryServiceImpl;
import com.rs.librarysystem.util.ResponseCode;
import com.rs.librarysystem.util.ResponseGenerator;
import com.rs.librarysystem.util.ResponseMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class LibraryServiceImplTest {

    @Mock
    private BorrowerRepository borrowerRepository;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private BorrowingRecordRepository borrowingRecordRepository;

    @Mock
    private BookCopyRepository bookCopyRepository;

    @Mock
    private ResponseGenerator responseGenerator;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private LibraryServiceImpl libraryService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testBorrowerRegistration() {
        BorrowerDTO borrowerDTO = new BorrowerDTO();
        borrowerDTO.setName("Rahul Satheesh");
        borrowerDTO.setEmail("rs@example.com");

        Borrower borrower = new Borrower();
        borrower.setName("Rahul Satheesh");
        borrower.setEmail("rs@example.com");

        when(borrowerRepository.findByEmail(any(String.class))).thenReturn(Optional.empty());
        when(modelMapper.map(any(BorrowerDTO.class), any(Class.class))).thenReturn(borrower);
        when(responseGenerator.generateResponse(eq(ResponseCode.SUCCESS), eq(ResponseMessage.BORROWER_REGISTRATION_SUCCESS_MSG), any(Object[].class), any(Locale.class)))
                .thenReturn(ResponseEntity.ok().body("Success"));

        ResponseEntity<Object> response = libraryService.borrowerRegistration(borrowerDTO, Locale.getDefault());

        assertEquals("Success", response.getBody());
    }

    @Test
    void testBorrowerRegistrationEmailAlreadyRegistered() {
        BorrowerDTO borrowerDTO = new BorrowerDTO();
        borrowerDTO.setName("Rahul Satheesh");
        borrowerDTO.setEmail("rs@example.com");

        Borrower existingBorrower = new Borrower();
        existingBorrower.setName("Rahul Satheesh");
        existingBorrower.setEmail("rs@example.com");

        when(borrowerRepository.findByEmail(any(String.class))).thenReturn(Optional.of(existingBorrower));
        when(responseGenerator.generateResponse(eq(ResponseCode.EMAIL_ALREADY_REGISTERED), eq(ResponseMessage.EMAIL_ALREADY_REGISTERED_MSG), any(Object[].class), any(Locale.class)))
                .thenReturn(ResponseEntity.badRequest().body("Email already registered"));

        ResponseEntity<Object> response = libraryService.borrowerRegistration(borrowerDTO, Locale.getDefault());

        assertEquals("Email already registered", response.getBody());
    }

    @Test
    void testRegisterBook() {
        BookDTO bookDTO = new BookDTO();
        bookDTO.setIsbn("123456789");
        bookDTO.setTitle("Spring Boot");
        bookDTO.setAuthor(1);
        bookDTO.setAvailable(5);

        Author author = new Author();
        author.setAuthorID(1);
        author.setName("Author Name");

        Book book = new Book();
        book.setIsbn("123456789");
        book.setTitle("Spring Boot");
        book.setAuthor(author);
        book.setAvailable(5);

        when(bookRepository.findByIsbn(any(String.class))).thenReturn(Optional.empty());
        when(authorRepository.findById(any(Integer.class))).thenReturn(Optional.of(author));
        when(modelMapper.map(any(BookDTO.class), any(Class.class))).thenReturn(book);
        when(responseGenerator.generateResponse(eq(ResponseCode.SUCCESS), eq(ResponseMessage.BOOK_REGISTRATION_SUCCESS_MSG), any(Object[].class), any(Locale.class)))
                .thenReturn(ResponseEntity.ok().body("Success"));

        ResponseEntity<Object> response = libraryService.registerBook(bookDTO, Locale.getDefault());

        assertEquals("Success", response.getBody());
    }

    @Test
    void testRegisterBookAuthorNotFound() {
        BookDTO bookDTO = new BookDTO();
        bookDTO.setIsbn("123456789");
        bookDTO.setTitle("Spring Boot");
        bookDTO.setAuthor(1);
        bookDTO.setAvailable(5);

        when(bookRepository.findByIsbn(any(String.class))).thenReturn(Optional.empty());
        when(authorRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        when(responseGenerator.generateResponse(eq(ResponseCode.INVALID_AUTHOR), eq(ResponseMessage.ERROR_INVALID_AUTHOR_DETAILS), any(Object[].class), any(Locale.class)))
                .thenReturn(ResponseEntity.badRequest().body("Invalid author details"));

        ResponseEntity<Object> response = libraryService.registerBook(bookDTO, Locale.getDefault());

        assertEquals("Invalid author details", response.getBody());
    }

//    @Test
//    void testBorrowBookSuccess() {
//        Book book = new Book();
//        book.setBookID(1);
//        book.setAvailable(1);
//
//        Borrower borrower = new Borrower();
//        borrower.setBorrowerID(1);
//
//        BookCopy bookCopy = new BookCopy();
//        bookCopy.setBook(book);
//        bookCopy.setStatus(BookCopyStatus.AVAILABLE);
//
//        BorrowingRecord borrowingRecord = new BorrowingRecord();
//        borrowingRecord.setBorrower(borrower);
//        borrowingRecord.setBookCopy(bookCopy);
//        borrowingRecord.setBorrowedDate(new Date());
//        borrowingRecord.setDueDate(new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(14))); // 14 days
//
//        when(bookRepository.findById(any(Integer.class))).thenReturn(Optional.of(book));
//        when(borrowerRepository.findById(any(Integer.class))).thenReturn(Optional.of(borrower));
//        when(bookCopyRepository.findFirstByBookAndStatus(any(Book.class), eq(BookCopyStatus.AVAILABLE))).thenReturn(Optional.of(bookCopy));
//        when(borrowingRecordRepository.save(any(BorrowingRecord.class))).thenReturn(borrowingRecord);
//        when(responseGenerator.generateResponse(eq(ResponseCode.SUCCESS), eq(ResponseMessage.BOOK_BORROWED_SUCCESS_MSG), any(Object[].class), any(Locale.class)))
//                .thenReturn(ResponseEntity.ok().body("Success"));
//
//        ResponseEntity<Object> response = libraryService.borrowBook(1, 1, Locale.getDefault());
//
//        assertEquals("Success", response.getBody());
//    }

    @Test
    void testBorrowBookLimitExceeded() {
        Book book = new Book();
        book.setBookID(1);
        book.setAvailable(1);

        Borrower borrower = new Borrower();
        borrower.setBorrowerID(1);

        when(bookRepository.findById(any(Integer.class))).thenReturn(Optional.of(book));
        when(borrowerRepository.findById(any(Integer.class))).thenReturn(Optional.of(borrower));
        when(borrowingRecordRepository.countBorrowedBooksByBorrower(any(Integer.class))).thenReturn(5); // assume borrow limit is 5
        when(responseGenerator.generateResponse(eq(ResponseCode.LIMIT_EXCEEDED), eq(ResponseMessage.BORROW_LIMIT_EXCEEDED), any(Object[].class), any(Locale.class)))
                .thenReturn(ResponseEntity.badRequest().body("Borrow limit exceeded"));

        ResponseEntity<Object> response = libraryService.borrowBook(1, 1, Locale.getDefault());

        assertEquals("Borrow limit exceeded", response.getBody());
    }

    @Test
    void testReturnBookSuccess() {
        BorrowingRecord borrowingRecord = new BorrowingRecord();
        borrowingRecord.setRecordID(1);
        borrowingRecord.setReturnedDate(new Date());

        BookCopy bookCopy = new BookCopy();
        bookCopy.setStatus(BookCopyStatus.BORROWED);

        Book book = new Book();
        book.setAvailable(0);

        borrowingRecord.setBookCopy(bookCopy);
        bookCopy.setBook(book);

        when(borrowingRecordRepository.findById(any(Integer.class))).thenReturn(Optional.of(borrowingRecord));
        when(responseGenerator.generateResponse(eq(ResponseCode.SUCCESS), eq(ResponseMessage.BOOK_RETURNED_SUCCESS_MSG), any(Object[].class), any(Locale.class)))
                .thenReturn(ResponseEntity.ok().body("Success"));

        ResponseEntity<Object> response = libraryService.returnBook(1, Locale.getDefault());

        assertEquals("Success", response.getBody());
    }

    @Test
    void testReturnBookInvalidRecord() {
        when(borrowingRecordRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        when(responseGenerator.generateResponse(eq(ResponseCode.INVALID_RECORD), eq(ResponseMessage.INVALID_RECORD_MSG), any(Object[].class), any(Locale.class)))
                .thenReturn(ResponseEntity.badRequest().body("Invalid record"));

        ResponseEntity<Object> response = libraryService.returnBook(1, Locale.getDefault());

        assertEquals("Invalid record", response.getBody());
    }
}