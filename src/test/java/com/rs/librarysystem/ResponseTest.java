package com.rs.librarysystem;

import com.rs.librarysystem.domain.Response;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ResponseTest {

    @Test
    void testResponse() {
        Response response = new Response();
        response.setStatus("200");
        response.setMessage("Success");

        assertEquals("200", response.getStatus());
        assertEquals("Success", response.getMessage());
    }

    @Test
    void testResponseEqualsHashCode() {
        Response response1 = new Response("200", "Success");
        Response response2 = new Response("200", "Success");

        assertEquals(response1, response2);
        assertEquals(response1.hashCode(), response2.hashCode());
    }
}
